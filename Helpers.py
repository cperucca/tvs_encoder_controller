import urllib2, base64
from xml.dom import minidom
import xml.etree.ElementTree as ET
import codecs
import datetime
import operator
import os.path

def CheckLimits(time, begintime, endtime ):
	if time < begintime or time > endtime:
		# devo chiudere se e' aperto
		if stato_rubinetto:
			# se e' acceso lo spengo
			#while the command:
			#http://10.72.10.27/rest/digitalItem/withdraw/742?publishPoint=3
			#will turn off the live Stream for RSI La Uno
			print ' Spengo il rubinetto '
			stato_rubinetto = False
			out = 'OFF'
			fin = open("/home/perucccl/pyStream/._state_stream_la1_tvsvizzera_.", 'w' )
			fin.write(out)
			fin.close()
			exit(0)
		else:
			# sono fuori dai limiti e il rubinetto e' chiuso
			# tutto a posto : esco e sto tranquillo
			exit(0)
			
def ScriviFA( start, end ):

	# adesso devo scrivere se sono acceso o spento
	# per eventualmente segnarmi il cambiamento di stato
	# non c'e' bisogno di lock perche' sono sempre solo
	# io a modificare lo stato
	fout = open("/home/perucccl/pyStream/._fa_stream_la1_tvsvizzera_.", 'w' )
	fout.write(start.strftime("%Y-%m-%dT%H:%M:%SZ" ))
	fout.write(',')
	fout.write(end.strftime("%Y-%m-%dT%H:%M:%SZ" ))
	fout.write('\n')
	fout.close()


def PrendiFA():

	# adesso devo guardare se ero acceso o spento
	#per eventualmente segnarmi il cambiamento di stato
	# non c'e' bisogno di lock perche' sono sempre solo
	#io a modificare lo stato
	fin = open("/home/perucccl/pyStream/._fa_stream_la1_tvsvizzera_.", 'r' )
	db_in = fin.read().splitlines()
	fin.close()

	fa = []
	print ' la finestra attuale e  : '
	for value in db_in:
		#print value
		start =  datetime.datetime.strptime( value.split(',')[0] ,"%Y-%m-%dT%H:%M:%SZ")  
		end =  datetime.datetime.strptime( value.split(',')[-1] ,"%Y-%m-%dT%H:%M:%SZ")  
		fa.append( [ start, end ]  )


	for fin in fa:
		print fin

	return fa

def ScriviStatoRubinetto( value ):

	# adesso devo scrivere se sono acceso o spento
	# per eventualmente segnarmi il cambiamento di stato
	# non c'e' bisogno di lock perche' sono sempre solo
	# io a modificare lo stato
	fin = open("/home/perucccl/pyStream/._state_stream_la1_tvsvizzera_.", 'w' )
	fin.write(value)
	fin.close()


def PrendiStatoRubinetto():

	# adesso devo guardare se ero acceso o spento
	#per eventualmente segnarmi il cambiamento di stato
	# non c'e' bisogno di lock perche' sono sempre solo
	#io a modificare lo stato
	fin = open("/home/perucccl/pyStream/._state_stream_la1_tvsvizzera_.", 'r' )
	stato_rubinetto = fin.read().splitlines()
	fin.close()

	print stato_rubinetto

	if 'ON' in stato_rubinetto:
		stato_rubinetto = True
	else:
		stato_rubinetto = False

	return stato_rubinetto



def PrendiDb( time ):
	# apri il db delle finestre
	# da mettere sotto lock del file .stream_la1_tvsvizzera.lock.
	date = time.strftime("%Y-%m-%d")

	#aspetto sul lock
	while os.path.isfile('/home/perucccl/pyStream/.stream_la1_tvsvizzera.lock.') : 
		print 'aspetto che finisca di lavorare il crea liste '

	if not os.path.exists("/home/perucccl/pyStream/._" + date + "_stream_la1_tvsvizzera_."):
		return None

	# prendo il lock
	open('/home/perucccl/pyStream/.stream_la1_tvsvizzera.lock.', 'a').close()

	# apro il db
	fin = codecs.open("/home/perucccl/pyStream/._" + date + "_stream_la1_tvsvizzera_.", 'r', 'utf-8')
	# leggo le finestre
	db_in = fin.read().splitlines()
	fin.close()
	# e rilascio il lock
	os.remove('/home/perucccl/pyStream/.stream_la1_tvsvizzera.lock.')

	db_finestre = []
	print ' il db conteneva  era : '
	for value in db_in:
		#print value
		start =  datetime.datetime.strptime( value.split(',')[0] ,"%Y-%m-%dT%H:%M:%SZ")  
		end =  datetime.datetime.strptime( value.split(',')[-1] ,"%Y-%m-%dT%H:%M:%SZ")  
		db_finestre.append( [ start, end ]  )


	for fin in db_finestre:
		print fin

	return db_finestre


def ScriviDb( louise_dict , time ):

	#aspetto sul lock
	while os.path.isfile('/home/perucccl/pyStream/.stream_la1_tvsvizzera.lock.') : 
		print 'aspetto che finisca di lavorare il rubinetto '
	# prendo il lock
	open('/home/perucccl/pyStream/.stream_la1_tvsvizzera.lock.', 'a').close()
	# e adesso salvala sul db delle finestre
	date =time.strftime("%Y-%m-%d")
	fout = codecs.open("/home/perucccl/pyStream/._" + date + "_stream_la1_tvsvizzera_.", 'w', 'utf-8')
	for lis in louise_dict:
		#print lis[0]
		fout.write(lis[0].strftime("%Y-%m-%dT%H:%M:%SZ" ))
		fout.write(',')
		fout.write(lis[1].strftime("%Y-%m-%dT%H:%M:%SZ" ))
		fout.write('\n')
	fout.close()
	# e rilascio il lock
	os.remove('/home/perucccl/pyStream/.stream_la1_tvsvizzera.lock.')


